from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
	url(r'^admin/',           include(admin.site.urls)),
	url(r'^admin/ajax/',      include('locking.urls')),
	url(r'^translator/',      include('django.conf.urls.i18n')),
	url(r'^tinymce/',         include('tinymce.urls')),
	url(r'^api/',             include('api.urls')),
	url(r'^cms',              include('cms.urls')),
	url(r'^comment',          include('comments.urls')),
	url(r'^feedback',         include('feedback.urls')),
	url(r'^id/',              include('accounts.urls')),
	url(r'^mailer/',          include('mailer.urls')),
	url(r'^polls/',           include('polls.urls')),
	url(r'404$',             'ninamori.views.not_found', name='404'),
	url(r'robots.txt$',      'ninamori.views.robots', name='robots'),
	url(r'sitemap.xml$',     'ninamori.views.sitemap', name='sitemap'),
	url(r'scripts.js$',      'ninamori.views.scripts', name='scripts'),
	url(r'stylesheet.css$',  'ninamori.views.stylesheet', name='stylesheet'),
	url(r'adminhelp$',       'ninamori.views.adminhelp', name='adminhelp'),
	url(r'^$',               'cms.views.index', name='index'),
	url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
]
