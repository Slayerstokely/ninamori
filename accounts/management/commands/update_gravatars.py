# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from accounts.utils import download_gravatar_if_updated


class Command(BaseCommand):
	help = 'Download updates for all gravatars'

	def add_arguments(self, parser):
		parser.add_argument('--rating',
			dest='rating',
			default=settings.GRAVATAR_RATING,
			help='maximum rating allowed')
		parser.add_argument('--default',
			dest='default',
			default=settings.GRAVATAR_DEFAULT,
			help='default gravatar type')
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Only count, do not download anything')
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Output only summary')
		parser.add_argument('--nosummary',
			action='store_true',
			dest='nosummary',
			default=False,
			help='do not output summary')
		parser.add_argument('--id',
			dest='id',
			default='',
			help='process just one user')

	def handle(self, *args, **options):
		update = not options['fake']
		rating = options['rating']
		default = options['default']
		silent = options['silent']
		nosummary = options['nosummary']
		user_id = options['id']
		counter_users = 0
		counter_updates = 0

		user_list = [User.objects.get(id=user_id)] if user_id else User.objects.all()

		for user in user_list:
			counter_users += 1
			updated = False
			if not silent: self.stdout.write('[' + str(counter_users) + '/' + str(len(user_list)) + '] updating gravatars for [' + user.username + '] with email address [' + user.email + ']')
			gravatar = (user.email, rating, default)
			gravatars = download_gravatar_if_updated(gravatar, update)
			for file_path, download in gravatars:
				if not silent: self.stdout.write('gravatars is [' + file_path + '] ' + ('' if download else '(cached)'))
				counter_updates += 1 if download else 0

		if not nosummary: self.stdout.write('Processed '+ str(counter_users) +' users, updated ' + str(counter_updates) + ' gravatars')
