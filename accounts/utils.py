# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext as _
from django.utils.translation import get_language
import django.utils.timezone
import datetime
import hashlib
import locale
import urllib
from os import stat, path
from time import strftime, localtime, mktime, strptime
from urllib2 import Request, urlopen
from mailqueue.models import MailerMessage
from ninamori.utils import validate_email, is_email_access_validated, download_file
from models import UserProfile


def adjust_comments_next_notification_date (userprofile, delay_interval):
	userprofile.comments_next_notification_date -= userprofile.settings_comments_delay_interval
	userprofile.comments_next_notification_date += delay_interval
	userprofile.settings_comments_delay_interval = delay_interval
	userprofile.save()


def api_register (username, email, password):
	if len(User.objects.filter(username=username)) != 0:
		return {
			'status': 'error',
			'result': 'username exists'
		}
	if not validate_email(email):
		return {
			'status': 'error',
			'result': 'invalid email'
		}
	if len(User.objects.filter(email=email)) != 0:
		return {
			'status': 'error',
			'result': 'email exists'
		}
	user = User.objects.create_user(username, email, password)
	download_gravatar_if_updated((email, settings.GRAVATAR_RATING, settings.GRAVATAR_DEFAULT), True)
	make_and_send_validation_code(user)  # this should also make sure user.userprofile is created before first login
	return {
		'status': 'ok',
		'result': 'user ' + username + ' created'
	}


def get_data_for_email (user, key=None, code=None):
	return {
		'protocol': settings.PROTOCOL_SCHEME,
		'site_domain': settings.SITE_DOMAIN,
		'app_index': reverse('accounts.views.index'),
		'app_validate': reverse('accounts.views.validate_email'),
		'code': user.userprofile.validation_email_code,
		'key': key,
		'restore_code': code,
		'username': user.username,
	}


def make_and_send_validation_code (user):
	if is_email_access_validated(user.email):
		user.userprofile.validation_email_valid = True
		title = _('[%(site_name)s] Successful registration' % {'site_name': settings.SITE_NAME[get_language()]})
		template = 'accounts/registration.txt'
	if not user.userprofile.validation_email_valid:
		user.userprofile.validation_email_sent = django.utils.timezone.now()
		user.userprofile.validation_email_code = str(user.id) + get_random_string(length=64)
		title = _('[%(site_name)s] Email address validation' % {'site_name': settings.SITE_NAME[get_language()]})
		template = 'accounts/email_validation.txt'

	user.userprofile.save()
	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, get_data_for_email(user))
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = user.email
	email_message.app           = 'accounts validation code'
	email_message.save()


def make_and_send_authorization_key (user, email=None):
	authorization_sha = hashlib.sha1(str(user.id)).hexdigest()
	authorization_key = authorization_sha + get_random_string(length=128)
	user.userprofile.authorization_sha = authorization_sha
	user.userprofile.authorization_key = make_password(authorization_key)
	user.userprofile.save()

	title = _('[%(site_name)s] Authorization key' % {'site_name': settings.SITE_NAME[get_language()]})
	template = 'accounts/authorization_key.txt'
	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, get_data_for_email(user, key=authorization_key))
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = email or user.email
	email_message.app           = 'accounts authorization code'
	email_message.save()

	return authorization_key


def make_and_send_restoration_key (user):
	restore_code = hashlib.sha1(str(user.id)).hexdigest() + get_random_string(length=200)
	user.userprofile.restore_code = make_password(restore_code)
	user.userprofile.restore_request_made = django.utils.timezone.now()
	user.userprofile.save()
	title = _('[%(site_name)s] Account restoration requested' % {'site_name': settings.SITE_NAME[get_language()]})
	template = 'accounts/restoration_code.txt'
	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, get_data_for_email(user, code=restore_code))
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = user.email
	email_message.app           = 'accounts restoration key'
	email_message.save()

	return restore_code


def get_gravatar_hash (email):
	return hashlib.md5(email.lower()).hexdigest()


def get_gravatar_url (gravatar, size):
	email = gravatar[0]
	rating = gravatar[1]
	default = gravatar[2]
	gravatar_url = 'https://www.gravatar.com/avatar/' + get_gravatar_hash(email) + '?'
	gravatar_url += urllib.urlencode({'d': default, 's': str(size), 'r': rating})
	return gravatar_url


def get_gravatar_file_path (email_hash, size):
	return settings.MEDIA_ROOT + '/avatars/gravatar_cache/' + email_hash + '_' + str(size) + '.png'


def download_gravatar_if_updated (gravatar, update):
	# setlocale here is fine because it should always be en_US.UTF-8
	locale.setlocale(locale.LC_ALL, b'en_US.UTF-8')
	gravatars = []
	for size in settings.GRAVATAR_SIZES:
		file_path = get_gravatar_file_path(get_gravatar_hash(gravatar[0]),size)
		url = get_gravatar_url(gravatar, size)
		if path.exists(file_path):
			updated = stat(file_path).st_mtime
			request = urlopen(Request(url))
			modified = mktime(strptime(request.info().getheader("Last-Modified"), "%a, %d %b %Y %H:%M:%S GMT"))
			download = modified > updated
		else:
			download = True
		if download and update:
			download_file(url, file_path)
		gravatars.append((file_path, download))

	return gravatars


def restore (username, email, request, delay):
	try:
		if username:
			user = User.objects.get(username=username)
		elif email:
			user = User.objects.get(email=email)
	except User.DoesNotExist:
		return False

	if username and email:
		try:
			assert user.username == username
			assert user.email == email
		except AssertionError:
			return False

	if user.userprofile.restore_request_made:
		if (django.utils.timezone.now() - user.userprofile.restore_request_made).total_seconds() < delay:
			return False

	restore_code = make_and_send_restoration_key(user)
	return {'user_id': user.id, 'code': restore_code}


def veryfy_code (code, str_id, delay):
	try:
		userprofile = UserProfile.objects.get(user_id=str_id)
		assert check_password(code, userprofile.restore_code) == True
		assert (django.utils.timezone.now() - userprofile.restore_request_made).total_seconds() < delay
	except (UserProfile.DoesNotExist, ValueError, AssertionError):
		return False

	return True
