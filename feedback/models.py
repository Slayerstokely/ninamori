# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.db import models


class Channel (models.Model):
	title_en = models.CharField(max_length=254, default="Feedback channel")
	title_ru = models.CharField(max_length=254, default="Канал поддержки")
	support_email = models.CharField(max_length=254, default="")

	def localized_title (self, value):
		if value == 'ru' and self.title_ru: return self.title_ru
		if value == 'en' and self.title_en: return self.title_en
		if settings.LANGUAGE_CODE == 'ru' and self.title_ru: return self.title_ru
		if settings.LANGUAGE_CODE == 'en' and self.title_en: return self.title_en
		if self.title_ru: return self.title_ru
		if self.title_en: return self.title_en
		return ''
