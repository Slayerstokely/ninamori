from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
	url(r'^$', 'feedback.views.index', name='feedback'),
	url(r'/send$', 'feedback.views.send', name='feedback'),
)
