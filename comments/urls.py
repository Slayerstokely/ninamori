from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
	url(r'^$', 'comments.views.comment', name='comment'),
	url(r'/moderate$', 'comments.views.moderate', name='moderate'),
	url(r'/edit$', 'comments.views.edit', name='edit'),
	url(r'/watch$', 'comments.views.watch', name='watch'),
	url(r'/watch_group$', 'comments.views.watch_group', name='watch_group'),
	url(r'/unfold$', 'comments.views.unfold', name='unfold'),
	url(r'/unfold_ajax$', 'comments.views.unfold_ajax', name='unfold ajax'),
	url(r'/ajax_branch$', 'comments.views.ajax_branch', name='ajax branch'),
)
