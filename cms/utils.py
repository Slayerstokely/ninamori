# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.template.defaultfilters import date as template_date
from django.utils.translation import activate as activate_translation
from math import ceil
from models import LeafletMap, Page
from ninamori.utils import cache_add, cache_get
from ninamori.templatetags.global_tags import minify_include_js
import datetime
import calendar
import json

cache_settings = settings.CACHES['default']


def week_of_month (date):
	first_day = date.replace(day=1)
	dom = date.day
	adjusted_dom = dom + first_day.weekday()
	return int(ceil(adjusted_dom/7.0))


def nth_weekday(date, nth_week, weekday):
		first_weekday_of_month      = date.replace(day=1)
		timedelta_for_weekday       = (weekday - first_weekday_of_month.weekday()) % 7
		weekday_first_occurrence    = first_weekday_of_month + timedelta(days=timedelta_for_weekday)
		nth_weekday                 = weekday_first_occurrence + timedelta(weeks=nth_week-1)
		return nth_weekday


def get_next_2nd_friday_or_last_saturday (language, date_format):
	#try-except to not touch cache more than needed
	try:
		result = cache_get('next_2nd_friday_or_last_saturday')
		assert(result)
	except AssertionError:
		today = datetime.datetime.today()
		result = next_2nd_friday_or_last_saturday(today)
		cache_add('next_2nd_friday_or_last_saturday', result, cache_settings['SHORT_TIMEOUT'])

	activate_translation(language)
	return template_date(result, date_format)


def next_2nd_friday_or_last_saturday (day):
	second_friday = nth_weekday(day, 2, 4)
	if day <= second_friday:
		# meetup is on second friday
		return second_friday

	calendar_month = calendar.monthcalendar(day.year, day.month)
	saturday = calendar_month[-1][5] or calendar_month[-2][5]
	last_saturday = datetime.datetime(year=day.year, month=day.month, day=saturday)
	if day <= last_saturday:
		# meetup is on last saturday
		return last_saturday

	if day.month < 12:
		month = day.month+1
		year = day.year
	else:
		month = 1
		year = day.year+1
	next_month_day = datetime.datetime(year=year, month=month, day=1)
	next_second_friday = nth_weekday(next_month_day, 2, 4)
	# meetup is on second friday of next month
	return next_second_friday


def get_hooks (content):
	result = []
	within_hook = False
	hook_start = 0
	for i in xrange(1, len(content)):
		if content[i] == content[i-1] == '{':
			within_hook = True
			hook_start = i-1
		if content[i] == content[i-1] == '}' and within_hook:
			within_hook = False
			result.append(content[hook_start:i+1])
	return result


def get_leaflet_map (title):
	try:
		result = cache_get('leaflet_map_' + title)
		assert(result)
	except AssertionError:
		try:
			leaflet_map = LeafletMap.objects.get(title=title)
		except LeafletMap.DoesNotExist:
			return 'leaflet map object is missing'
		try:
			assert type(leaflet_map.center) == list
			assert type(leaflet_map.center[0]) == float
			assert type(leaflet_map.center[1]) == float
			assert type(leaflet_map.marker) == list
			assert type(leaflet_map.marker[0]) == float
			assert type(leaflet_map.marker[1]) == float
			assert type(leaflet_map.waypoints) == list
			for i in range(0, len(leaflet_map.waypoints)):
				assert type(leaflet_map.waypoints[i][0]) == float
				assert type(leaflet_map.waypoints[i][1]) == float
				#somehow latitude and longtitude switches its places in waypoints for reason yet unknown
				leaflet_map.waypoints[i] = [leaflet_map.waypoints[i][1], leaflet_map.waypoints[i][0]]
		except Exception as e:
			return e.message
		html = render_to_string('cms/leaflet_map.html', {'title': title})
		js = minify_include_js('cms/leaflet_map.js', {
			'title': title,
			'center': json.dumps(leaflet_map.center),
			'marker': json.dumps(leaflet_map.marker),
			'waypoints': json.dumps(leaflet_map.waypoints),
			'zoom': leaflet_map.initial_zoom
		})
		result = (html, js)
		cache_add('leaflet_map_' + title, result, cache_settings['TIMEOUT'])

	return result


def get_page_last_modified (request, alias):
	page = get_object_or_404(Page, alias=alias)
	return page.date_modified
