from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
	url(r'^$', 'polls.views.vote', name='index'),
	url(r'activate_token$', 'polls.views.activate_token', name='activate token'),
	url(r'generate_tokens$', 'polls.views.generate_tokens', name='generate tokens'),
)
